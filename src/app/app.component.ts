import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  test: ITest = { uno: 1, dos: 'dos' };

  getParametro() {
    let otro: ITest;
    otro = { uno: this.test.uno, dos: this.test.dos};
    console.log('Enviado: ' + otro + ':' + otro.uno + ' ' + otro.dos);
    return otro;
  }
}

export interface ITest {
  uno: number;
  dos: string;
}
