import { Component, OnInit, Input } from '@angular/core';
import { ITest } from '../app.component';

@Component({
  selector: 'app-receptor',
  templateUrl: './receptor.component.html',
  styleUrls: ['./receptor.component.css']
})
export class ReceptorComponent implements OnInit {
  @Input() parametro: any;
  test: ITest;
  constructor() { }

  ngOnInit() {
    this.test = this.parametro as ITest;
    console.log('Recibido parametro: ' + this.parametro + ':' + this.parametro.uno + ' ' + this.parametro.dos);
    console.log('Recibido test: ' + this.test + ':' + this.test.uno + ' ' + this.test.dos);
  }

}
